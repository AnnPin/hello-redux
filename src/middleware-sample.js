import {createStore, applyMiddleware} from 'redux';

// middleware にする関数. 引数として store、本来の dispatch 関数、store.dispatch() 関数に渡された action を取る.
// この関数はカリー化されており、内部的には myMiddleware(store)(dispatch)(action) のように呼び出される.
const myMiddleware = store => next => action => {
  action.then(res => {
    console.log('Promise 中から取り出された Action:', res);
    console.log('Reducer 呼び出し前の state:', store.getState());
    next(res);
    console.log('Reducer 呼び出し後の state:', store.getState());
  });
};

// Action Creator 関数. 今回は Promise を返すためそのまま dispatch できない Action となる.
const addCount = async (num) => {
  return { type: 'ADD_COUNTER', value: num };
};

const reducer = (state = 0, action) => {
  switch (action.type) {
    case 'ADD_COUNTER':
      return state + action.value;
    default:
      return state;
  }
};

const main = () => {
  // applyMiddleware 関数を用いて middleware を作成し、createStore に渡す.
  const store = createStore(reducer, applyMiddleware(myMiddleware));

  store.dispatch(addCount(1));
  store.dispatch(addCount(10));
  store.dispatch(addCount(100));
  store.dispatch(addCount(1000));
  store.dispatch(addCount(10000));
};

main();

