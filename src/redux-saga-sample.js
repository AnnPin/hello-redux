import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import fetch from 'node-fetch';

// fetch 関数を用いて指定された URL から文字列を取得してくる非同期処理.
const fetchData = async (url) => {
  const response = await fetch(url);
  const result = response.text();
  return result;
};

// ワーカー saga. FETCH_REQUESTED アクションが発行されると実行される.
function* fetchDataSaga(action) {
  try {
    const content = yield call(fetchData, action.payload.url);
    // 取得した HTML 文字列の長さを Action にセットして dispatch する.
    yield put({type: 'FETCH_SUCCEEDED', bytes: content.length});
  } catch (e) {
    yield put({type: 'FETCH_FAILED', message: e.message});
  }
}

// 各 FETCH_REQUESTED アクションが dispatch されると fetchUser を実行する.
// このとき、非同期 fetch を行うことができる.
function* mySaga() {
  yield takeEvery('FETCH_REQUESTED', fetchDataSaga);
}

const reducer = (state = 'initial state', action) => {
  console.log('Action:', action);
  console.log('State:', `'${state}'`);
  console.log('-----');

  switch (action.type) {
    case 'FETCH_REQUESTED':
      return 'fetch requested and waiting for the asynchronous task...';

    case 'FETCH_SUCCEEDED':
      return 'fetch succeeded';

    case 'FETCH_FAILED':
      return 'fetch failed';

    default:
      return state;
  }
};

// Saga middleware を作成する.
const sagaMiddleware = createSagaMiddleware()

const main = () => {
  // Store を作成する.
  const store = createStore(reducer, applyMiddleware(sagaMiddleware));

  // Saga を実行する.
  sagaMiddleware.run(mySaga);

  // Action を dispatch し、sagaMiddleware を呼び出す.
  store.dispatch({ type: 'FETCH_REQUESTED', payload: { url: 'https://yahoo.co.jp' } });

  store.dispatch({ type: 'FETCH_REQUESTED', payload: { url: 'https://google.com' } });
};

main();

