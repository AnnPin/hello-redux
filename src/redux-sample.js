import { createStore } from 'redux';

// Reducer 関数.
const counter = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default:
      return state;
  }
}

const main = () => {
  // Store の作成.
  const initialState = 0;
  const store = createStore(counter, initialState);

  store.subscribe(() => {
    console.log(store.getState());
  });

  store.dispatch({ type: 'INCREMENT' });
  store.dispatch({ type: 'INCREMENT' });
  store.dispatch({ type: 'DECREMENT' });
};

main();

