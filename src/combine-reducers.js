import { createStore, combineReducers } from 'redux';

// Reducers
const todosReducer = (state = [], action) => {
  // state は配列.
  switch (action.type) {
    case 'ADD_TODO':
      // 新たな todo を配列の末尾に付与して state として返す.
      return state.concat([action.text]);
    default:
      return state;
  }
};

const counterReducer = (state = 0, action) => {
  // state は整数値.
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default:
      return state;
  }
};

// combineReducers 関数を使い、トップレベル Reducer 関数を作成.
const reducer = combineReducers({
  todos: todosReducer,
  counter: counterReducer
});

const main = () => {
  // Store 作成.
  const store = createStore(reducer);
  const logger = () => {
    console.log(store.getState());
  };
  logger();

  store.subscribe(logger);

  store.dispatch({ type: 'ADD_TODO', text: 'Use Redux' });
  store.dispatch({ type: 'INCREMENT' });
  store.dispatch({ type: 'ADD_TODO', text: 'Use React' });
  store.dispatch({ type: 'DECREMENT' });
};

main();

