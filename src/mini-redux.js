// ライブラリゼロの 20 行で Redux もどきを実装して、Redux を完全掌握しよう！！
// <https://zenn.dev/harukaeru/articles/45d2579493a5c4>

const createStore = (initialState, reducer) => {
  let state = initialState;
  const listeners = [];

  const dispatch = (action) => {
    const newState = reducer(state, action);
    state = newState;
    listeners.map(listener => listener());
  };

  const subscribe = (listener) => {
    listeners.push(listener);
  };

  return {
    dispatch: dispatch,
    getState: () => state,
    subscribe: subscribe
  };
};

const main = () => {
  // Reducer としては、'increment' か 'decrement' のアクションを受け取り、それに応じて
  // state の値を +1 or -1 することで新たな state を作るという関数を使う.
  const reducer = (state, action) => {
    switch (action.type) {
      case 'increment':
        return state + 1;
      case 'decrement':
        return state - 1;
    }
  };

  // Store を作成する.
  const store = createStore(0, reducer);

  // ロガー. 後で subscribe するため、listener として実行される.
  const logger = () => {
    // reducer でステートが更新されたあとに自動実行され、
    // 更新後のステートの値が表示される.
    console.log(store.getState());
  };

  store.subscribe(logger);

  store.dispatch({ type: 'increment' });
  store.dispatch({ type: 'increment' });
  store.dispatch({ type: 'increment' });
  store.dispatch({ type: 'increment' });
  store.dispatch({ type: 'increment' });

  store.dispatch({ type: 'decrement' });
  store.dispatch({ type: 'decrement' });
  store.dispatch({ type: 'decrement' });
  store.dispatch({ type: 'decrement' });
  store.dispatch({ type: 'decrement' });
};

main();

