import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const sleep = (milliSeconds: number) => {
  return new Promise<void>(resolve => {
    setTimeout(() => {
      resolve();
    }, milliSeconds);
  });
};

export const addTodo = createAsyncThunk(
  'todo/addTodo',
  async (content: string) => {
    await sleep(1000);
    return content;
  }
);

interface TodoItem {
  id: number,
  content: string
}

interface TodoState {
  loading: boolean,
  error: boolean,
  todoList: TodoItem[]
}

const initialState: TodoState = {
  loading: false,
  error: false,
  todoList: []
};

const todoSlicer = createSlice({
  name: 'todo',
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(addTodo.pending, (state) => {
      state.loading = true;
      state.error = false;
    });
    builder.addCase(addTodo.fulfilled, (state, action) => {
      state.loading = false;
      state.error = false;
      state.todoList.push(
        {
          id: state.todoList.length === 0 ? 1 : state.todoList[state.todoList.length - 1].id + 1,
          content: action.payload
        }
      );
    });
    builder.addCase(addTodo.rejected, (state) => {
      state.loading = false;
      state.error = true;
    });
  }
});

export default todoSlicer.reducer;

