import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './App.css';
import { RootState } from './store';
import { increment, decrement } from './store/counter/action';

function App() {
  // useSelector / useDispatch の使用例.
  // react-redux では従来 connect 関数を使い、その中で mapStateToProps や
  // mapDispatchToProps を設定していた. しかし、React Hooks が登場してからは
  // react-redux では connect/mapStateToProps/mapDispatchToProps の代わりに
  // react-redux が提供している useSelector フックや useDispatch フックを使うことを
  // 推奨している.

  // 例えば、以下では mapStateToProps の代わりに useSelector() フックを使用することで
  // state から値を取り出す selector を記述している.
  // 同様にして、useDispatch() フックを使用することで Store から dispatch 関数への参照を
  // 取得することができるため、mapDispatchToProps の代わりとして使用することができる.
  const countState = useSelector((state: RootState) => state.count);
  const dispatch = useDispatch();

  const onIncrement = () => {
    dispatch(increment());
  };

  const onDecrement = () => {
    dispatch(decrement())
  };

  return (
    <div>
      { countState.value }
      <button onClick={onIncrement}>+1</button>
      <button onClick={onDecrement}>-1</button>
    </div>
  );
}

export default App;

