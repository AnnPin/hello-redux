import { createSlice } from '@reduxjs/toolkit';

// createSlice 関数で Slice を作成する.
export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    count: 0
  },
  reducers: {
    increase: (state) => {
      state.count += 1;
    },
    decrease: (state) => {
      state.count -= 1;
    }
  }
});

// Slice に登録した Reducer の情報から対応する Reducer を呼び出す Action を発行する
// Action Creator 関数を自動作成.
export const { increase, decrease } = counterSlice.actions;

// Reducer 関数を default export する.
export default counterSlice.reducer;

