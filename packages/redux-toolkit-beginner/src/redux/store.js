import { configureStore } from '@reduxjs/toolkit';
// Slice で定義した Reducer 関数群 (key に Reducer 名、値に関数が登録されたオブジェクト)
import counterReducer from './counterSlice';
import userReducer from './userSlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    user: userReducer,
  }
});

