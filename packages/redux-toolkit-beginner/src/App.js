import './App.css';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { increase, decrease } from './redux/counterSlice';
import { getUsers } from './redux/userSlice';

function App() {
  // useSelector フックで Selector 関数を登録.
  const count = useSelector((state) => state.counter.count);
  const { users, loading, error } = useSelector((state) => state.user);

  // useDispatch フックで dispatch 関数を取得.
  const dispatch = useDispatch();

  // useEffect フックで非同期処理をキック.
  useEffect(() => {
    // dispatch 関数に (dispatch) => Promise<T> 型の関数を引数として渡す.
    dispatch(getUsers());
  }, [dispatch]);

  return (
    <div className="App">
      <h1>Count: {count}</h1>

      <button onClick={() => dispatch(increase())}>Up</button>
      <button onClick={() => dispatch(decrease())}>Down</button>

      <h2>User</h2>
      {loading && <p>Loading</p>}
      {error && <p>データ取得に失敗しました.</p>}
      {users && users.map((user, index) => (
        <div key={index}>
          {user.name}
        </div>
      ))}
    </div>
  );
}

export default App;

