import { useState } from 'react';
import { useAppSelector, useAppDispatch } from './redux/hooks';
import { addTodo } from './redux/todoSlice';
import './App.css';

function App() {
  const todoList = useAppSelector(state => state.todo.todoList);
  const dispatch = useAppDispatch();

  const [content, setContent] = useState('');

  return (
    <div>
      <input type="text"
             value={content}
             onChange={(e) => setContent(e.currentTarget.value)}/>
      <button onClick={() => {
        dispatch(addTodo(content))
        setContent('');
      }}>
        Add
      </button>

      <ul>
        {todoList && todoList.map(todo => (
          <li key={todo.id}>{todo.content}</li>
        ))}
      </ul>
    </div>
  );
}

export default App;

