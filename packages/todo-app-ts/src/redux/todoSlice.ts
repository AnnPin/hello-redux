import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

interface TodoItem {
  id: number;
  content: string;
}

interface TodoState {
  todoList: TodoItem[]
}

const initialState: TodoState = {
  todoList: []
};

export const todoSlice = createSlice({
  name: 'todo',
  initialState: initialState,
  reducers: {
    addTodo: (state: TodoState, action: PayloadAction<string>) => {
      const lastId = state.todoList.length === 0
        ? 0
        : state.todoList[state.todoList.length - 1].id;
      state.todoList.push({
        id: lastId + 1,
        content: action.payload
      });
    }
  }
});

export const { addTodo } = todoSlice.actions;

export default todoSlice.reducer;

