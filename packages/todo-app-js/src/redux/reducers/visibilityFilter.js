import { SET_FILTER } from '../actionTypes';
import { VISIBILITY_FILTERS } from '../../constants';

const initialState = VISIBILITY_FILTERS.ALL;

const visibilityFilter = (state = initialState, action) => {
  switch (action.type) {
    case SET_FILTER: {
      // SET_FILTER Action を送出するのは VisibilityFilters コンポーネント.
      // 内部で setFilter Action Creator を実行するので、
      // actions.js の setFilter 関数を読めばよい.
      return action.payload.filter;
    }
    default:
      return state;
  }
};

export default visibilityFilter;

