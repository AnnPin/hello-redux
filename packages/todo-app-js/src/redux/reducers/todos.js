import { ADD_TODO, TOGGLE_TODO } from '../actionTypes';

const initialState = {
  allIds: [],
  byIds: {}
};

// TodoList のための Reducer 関数.
export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO: {
      // ADD_TODO Action を送出するのは InputWithButton コンポーネント.
      // 内部で addTodo Action Creator を実行するので、
      // actions.js の addTodo 関数を読めばよい.
      const { id, content } = action.payload;
      return {
        ...state,
        // allIds に新たな id を追加.
        allIds: [...state.allIds, id],
        byIds: {
          ...state.byIds,
          [id]: {
            content,
            completed: false
          }
        }
      };
    }
    case TOGGLE_TODO: {
      // TOGGLE_TODO Action を送出するのは TodoItem コンポーネント.
      // 内部で toggleTodo Action Creator を実行するので、
      // actions.js の toggleTodo 関数を読めばよい.
      const { id } = action.payload;
      return {
        ...state,
        byIds: {
          ...state.byIds,
          [id]: {
            ...state.byIds[id],
            completed: !state.byIds[id].completed
          }
        }
      };
    }
    default:
      return state;
  }
}

