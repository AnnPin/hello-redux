import { VISIBILITY_FILTERS } from '../constants';

// state 中から todo 情報だけ取り出す.
const getTodosState = (state) => state.todos;

// state 中からすべての todo の id を取り出し配列で返す.
const getTodoList = (state) =>
  getTodosState(state) ? getTodosState(state).allIds : [];

// state 中から特定の id の todo 情報をオブジェクトとして返す.
const getTodoById = (state, id) =>
  getTodosState(state) ? { ...getTodosState(state).byIds[id], id } : {};

// state 中のすべての todo をオブジェクトの配列として返す.
const getTodos = (state) =>
  getTodoList(state).map(id => getTodoById(state, id));

// state 中から特定のフィルタに合致するすべての todo をオブジェクトの配列として返す.
export const getTodosByVisibilityFilter = (state, visibilityFilter) => {
  const allTodos = getTodos(state);
  switch (visibilityFilter) {
    case VISIBILITY_FILTERS.COMPLETED:
      return allTodos.filter(todo => todo.completed);
    case VISIBILITY_FILTERS.INCOMPLETE:
      return allTodos.filter(todo => !todo.completed);
    case VISIBILITY_FILTERS.ALL:
    default:
      return allTodos;
  }
};

