// 従来はファイル中で JSX を使用するために常に React のインポートが必要だったが、
// React 17 からは JSX から JS へのトランスフォーム処理が更新されこのインポートが不要になった.
// <https://ja.reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html>

// import React from 'react';
import InputWithButton from './components/InputWithButton';
import TodoList from './components/TodoList';
import VisibilityFilters from './components/VisibilityFilters';
import './styles.css';

export default function TodoApp() {
  return (
    <div className="todo-app">
      <h1>TODO リスト</h1>
      <InputWithButton />
      <TodoList />
      <VisibilityFilters />
    </div>
  );
}

