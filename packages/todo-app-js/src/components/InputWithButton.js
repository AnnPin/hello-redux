import { useState } from 'react';
import { connect } from 'react-redux';
import { addTodo } from '../redux/actions';

const InputWithButton = (props) => {
  const [input, setInput] = useState("");
  const updateInput = (input) => {
    setInput(input);
  };
  const handleAddTodo = () => {
    props.addTodo(input);
    setInput("");
  };

  return (
    <div>
      <input onChange={(e) => updateInput(e.target.value)}
             value={input} />
      <button className="add-todo" onClick={handleAddTodo}>
        追加
      </button>
    </div>
  );
};

export default connect(null, { addTodo: addTodo })(InputWithButton);

