import { connect } from 'react-redux';
import TodoItem from './TodoItem';
import { getTodosByVisibilityFilter } from '../redux/selectors';

const TodoList = ({ todos }) => (
  <ul className="todo-list">
    {todos && todos.length
      ? todos.map((todo, index) => {
          return <TodoItem key={`todo-${todo.id}`} todo={todo} />;
        })
      : 'タスクがありません。'
    }
  </ul>
);

const mapStateToProps = (state) => {
  // state 中から visibilityFilter を取り出す.
  const { visibilityFilter } = state;
  const todos = getTodosByVisibilityFilter(state, visibilityFilter);
  return {
    todos: todos
  };
};

export default connect(mapStateToProps)(TodoList);

