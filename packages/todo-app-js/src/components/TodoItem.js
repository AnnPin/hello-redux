import { connect } from 'react-redux';
import cx from 'classnames';
import { toggleTodo } from '../redux/actions';

const TodoItem = ({ todo, toggleTodo }) => (
  <li className='todo-item' onClick={() => toggleTodo(todo.id)}>
    {todo && todo.completed ? '👌' : '👋'}{' '}
    <span
      className={
        // 各 Todo 要素のスタイルを設定. 完了済みの Todo 要素に対しては追加でスタイルを指定.
        cx(
          'todo-item__text',
          todo && todo.completed && 'todo-item__text--completed'
        )
      }
    >
      {todo.content}
    </span>
  </li>
);

export default connect(null, { toggleTodo: toggleTodo })(TodoItem);

