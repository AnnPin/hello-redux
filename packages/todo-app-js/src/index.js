import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import reportWebVitals from './reportWebVitals';

// 自分で用意した Redux Store オブジェクト.
import store from './redux/store';

// トップレベルコンポーネント.
import TodoApp from './TodoApp';

// 従来は ReactDOM.render() 関数を呼び出していたが、
// 最近は代わりに ReactDOM.createRoot() でオブジェクトを作り、
// そのオブジェクトの .render() メソッドを呼び出すスタイルになったようだ.
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <TodoApp />
    </React.StrictMode>
  </Provider>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

